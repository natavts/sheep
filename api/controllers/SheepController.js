/**
 * SheepController sdf
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var hardcodeID = '54d490a1b88f9ccf072f1db1';

module.exports = {
  cut: function (req, res, next) {

    var id = req.param('id');

    Sheep.findOne(id, function cutSheep(err, sheep) {

      if (err) return next(err);
      if (!sheep) return next();

      var isoConvert = function(iso){
        return new Date(iso).getTime();
      };

      var timeout     =  1000*60* 5,
          lastDate    =  sheep.haircutDate ? isoConvert(sheep.haircutDate) : null,
          currentDate =  Date.now(),
          difference  =  currentDate - lastDate,
          nextDate    =  lastDate + timeout;

      var checkTimeout = function(){
        return currentDate >= nextDate;
      };

      var message, success;

      if (!lastDate || checkTimeout()){
        sheep.haircutDate = new Date(currentDate).toISOString();
        sheep.nextHaircut = new Date(currentDate+timeout).toISOString();
        sheep.save(function(err){ console.log(err)});
        success = true;

        var successCut = function() {
          User.findOne(req.session.User.id, function (err, user) {
            Action.create({
              type:'hairCut',
              status: 'in progress',
              actor: user.id,
              sheep: sheep.id,
              sheepOwner:sheep.owner
            }).exec(function createCB(err,created){
              console.log('Created action', created);
            });
            user.score = user.score ? user.score + 5 : 5;
            user.save(function (err) {
              console.log(err)
            });
          });
          console.log('начислены очки')
        };
        setTimeout(successCut(), 300000);


        message = 'пострижено'.toUpperCase();
      } else {
        var time = Math.round((timeout - difference) / (1000*60));
        var mes = time ? 'До следующей стрижки ' + time + ' минут' : 'Следующая стрижка через минуту';
        success = false;
        message = 'Нельзя! ' + mes;
      }

      return res.json({
        "success": success,
        "response": message,
        "sheep": sheep
      });

    });

  }
};
