/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  attributes: {
    username: {
      type: 'string',
      unique: true
    },
    fullname: {
      type: 'string'
    },
    avatar: 'string',
    role: 'string',
    score: 'integer',
    sheep: {
      collection: 'sheep',
      via: 'owner'
    }
  },

  toJSON: function () {
    var obj = this.toJSON();
    return obj;
  },
  afterCreate: function(values, cb){
    for(var i=0; i<2; i++){
      Sheep.create({name:'новая овечка', color: 'радужный', owner:values.id}).exec(function createCB(err,created){
        console.log('Created user with name '+created.name);
      });
    }
  }




  /*beforeCreate: function (values, next) {
    if (!values.password || values.password != values.confirmation){
      return next({err: ["Пароль не совпадает с подтверждением."]})
    }

    require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword){
      if(err) return next(err);

      values.encryptedPassword = encryptedPassword;
      values.online = true;
      next();
    });

  }*/
};

