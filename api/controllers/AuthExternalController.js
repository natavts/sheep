/**
 * AuthExternalController
 *
 * @description :: Server-side logic for managing authexternals
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var REDIRECT_PATH = '/front/#/profile';

module.exports = {
  index: function (req, res, next) {

    var userObj = JSON.parse(req.params[0]);

    var authenticated = function (req, user, path) {
      user.save(function (err, user) {
        if (err) {
          return next(err);
        }
        req.session.authenticated = true;
        req.session.User = user;
        res.redirect(path);
      });
    };

    User.findOne({username: userObj.username}).exec(function findUserByUsername(err, user) {
      if (err) {
        return next(err);
      }

      if (!user) {
        User.create(userObj, function userCreated(err, user) {
          if (err) {
            return next(err);
          }
          authenticated(req, user, REDIRECT_PATH);
        })
      } else {
        User.update(user.id, userObj, function userUpdated(err, updated) {
          if (err) {
            return next(err);
          }
          console.log(updated[0]);
          authenticated(req, updated[0], REDIRECT_PATH);
        });
      }
    })
  }
};

