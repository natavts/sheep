/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  'new': function (req, res) {
    res.locals.flash = _.clone(req.session.flash);
    res.view();
    req.session.flash = {};
  },

  'my': function(req, res) {
    res.json(req.session.User);
  },

  create: function (req, res, next) {
    var userObj = {};
    console.log(req.method);
    if (req.method === 'GET' && req.params[0]) {
      userObj = JSON.parse(req.params[0])
    } else {

      console.log(req.param('username'));
      userObj = {
        username: req.param('username'),
        fullname: req.param('fullname'),
        role: req.param('role'),
        avatar: req.param('avatar')
        //email: req.param('email'),
        //password: req.param('password'),
        //confirmation: req.param('confirmation')
      };
    }

    User.findOne({username: userObj.username}).exec(function findUserByUsername(err,user){

      if (err) {
        console.log('findUserByName', err);
        return next(err);
      }


      if (!user) {
        console.log('!user');
        console.log(userObj);
        User.create(userObj, function userCreated(err, user) {
          console.log('userCreated');
          if (err) {
            console.log('userCreated err')
            console.log(err);
            req.session.flash = {
              err: err
            };
            return res.redirect('user/new');
          }

          console.log('userCreated, Log user in');


          // Change status to online
          //user.online = true;
          user.save(function (err, user) {
            console.log('user.save');
            if (err) {
              console.log('save err', err);
              req.session.flash = {
                err: err
              };
              return res.redirect('user/new');
            }

            // Log user in
            req.session.authenticated = true;
            req.session.User = user;

            console.log('save ok');
            //user.action = " signed-up and logged-in."
            //User.publishCreate(user);
            res.redirect('/user');
          });
        })
      } else {
        // Log user in
        req.session.authenticated = true;
        req.session.User = user;
        //console.log(user);
        res.redirect('/user/' + user.id);

      }


    })


  }


  /*'create': function (req, res, next) {

   User.create(req.params.all(), function UserCreated(err, user) {

   console.log(req.params.all());

   if (err) {
   console.log(err);
   req.session.flash = {
   err: err
   };


   return res.redirect('user/new')
   }

   res.json(user);
   req.session.flash = {};
   });
   }*/


};

