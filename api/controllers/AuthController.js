/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  'external': function (req, res, next) {

    var userObj = JSON.parse(req.params[0]);

    User.findOne({username: userObj.username}).exec(function findUserByUsername(err, user) {
      if (err) {
        return next(err);
      }

      if (!user) {
        User.create(userObj, function userCreated(err, user) {
          if (err) {
            return next(err);
          }

          user.save(function (err, user) {
            if (err) {
              return next(err);
            }

            req.session.authenticated = true;
            req.session.User = user;

            res.redirect('/user');
          });
        })
      } else {
        req.session.authenticated = true;
        req.session.User = user;
        res.redirect('/user/' + user.id);

      }
    })
  }
};

